#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os, sys, pygtk, gtk.glade
import x_window

class Application:
	def __init__( self, args ):
	    win = x_window.Window( args )
	    win.show_all()
	    gtk.main()
