#!/usr/bin/env python
# -*- coding: utf-8 -*-


import pygtk, gtk.glade, gobject

class Control(gtk.VBox):
    __gsignals__ = {
        "append": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_STRING,)),
        "insert": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_STRING,gobject.TYPE_INT,gobject.TYPE_INT,)),
        "save": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, ()),
        "load": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, ()),
        "new-group": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, ()),
        "set-pos": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_INT,gobject.TYPE_INT,)),
    }

    def __init__( self ):
        gtk.VBox.__init__( self )
	self.L1 = gtk.HBox()
	self.L2 = gtk.HBox()
	frame_box = gtk.HBox()
	frame = gtk.Frame("Логические выражения:")
	self.L1.simple_set = True
	self.L2.simple_set = True
	self.L1.set_spacing(5) 
	self.L2.set_spacing(5) 

	self.x = 0
	self.y = 0
	self.x_tag = gtk.Entry()
	#self.x_tag.set_sensitive( False )
	self.y_tag = gtk.Entry()
        self.y_tag.connect("activate", self.on_put_at_pos_clicked)
	self.y_tag.set_icon_from_stock( gtk.ENTRY_ICON_PRIMARY, gtk.STOCK_NO )
	self.y_tag.set_icon_from_stock( gtk.ENTRY_ICON_SECONDARY, gtk.STOCK_CLEAR )
	self.y_tag.connect("icon-press", self.on_y_tag_clear)
	#self.y_tag.set_sensitive( False )
	self.pos_tag = gtk.Label("pos: last")
	self.table_id = ""

	move = gtk.Button()
	move.set_tooltip_text("Заменить значение в выранной ячейке")
        move.set_focus_on_click(False)
        move.add( gtk.image_new_from_stock( gtk.STOCK_MEDIA_FORWARD, gtk.ICON_SIZE_LARGE_TOOLBAR ) )
        move.connect("clicked", self.on_move_clicked)

	add_to_end = gtk.Button()
	add_to_end.set_tooltip_text("Добавить выбранный тег к выражению")
        add_to_end.set_focus_on_click(False)
        add_to_end.add( gtk.image_new_from_stock( gtk.STOCK_GOTO_LAST, gtk.ICON_SIZE_LARGE_TOOLBAR ) )
        add_to_end.connect("clicked", self.on_add_to_end_clicked)

	pos_frame = gtk.Frame( )
	pos_frame.set_shadow_type(gtk.SHADOW_ETCHED_OUT)
	pos_frame.add ( self.pos_tag )

	put_at_pos = gtk.Button()
	put_at_pos.set_tooltip_text("Записать тег(выражение) в ячейку")
        put_at_pos.set_focus_on_click(False)
        put_at_pos.add( gtk.image_new_from_stock( gtk.STOCK_GOTO_BOTTOM, gtk.ICON_SIZE_LARGE_TOOLBAR ) )
        put_at_pos.connect("clicked", self.on_put_at_pos_clicked)

	clear_pos = gtk.Button()
	clear_pos.set_tooltip_text("Очистить данную ячейку")
        clear_pos.set_focus_on_click(False)
        clear_pos.add( gtk.image_new_from_stock( gtk.STOCK_CUT, gtk.ICON_SIZE_LARGE_TOOLBAR ) )
        clear_pos.connect("clicked", self.on_clear_pos_clicked)

	new_group = gtk.Button()
	new_group.set_tooltip_text("Создать новую группу")
        new_group.set_focus_on_click(False)
        new_group.add( gtk.image_new_from_stock( gtk.STOCK_JUMP_TO, gtk.ICON_SIZE_LARGE_TOOLBAR ) )
        new_group.connect("clicked", self.on_new_group )

	logic_not = gtk.Button("    !    ")
	logic_not.set_tooltip_text("Вставить инверсию")
        logic_not.set_focus_on_click(False)
        #logic_not.add( gtk.image_new_from_stock( gtk.STOCK_CAPS_LOCK_WARNING, gtk.ICON_SIZE_LARGE_TOOLBAR ) )
        logic_not.connect("clicked", self.on_logic_not_clicked)

	logic_or = gtk.Button("   or   " )
	logic_or.set_tooltip_text("Вставить ИЛИ")
        logic_or.set_focus_on_click(False)
        #logic_or.add( gtk.image_new_from_stock( gtk.STOCK_MEDIA_PAUSE, gtk.ICON_SIZE_LARGE_TOOLBAR ) )
        logic_or.connect("clicked", self.on_logic_or_clicked)

	logic_and = gtk.Button("  and  ")
	logic_and.set_tooltip_text("Вставить И")
        logic_and.set_focus_on_click(False)
        #logic_and.add( gtk.image_new_from_stock( gtk.STOCK_NO, gtk.ICON_SIZE_LARGE_TOOLBAR ) )
        logic_and.connect("clicked", self.on_logic_and_clicked)

	open_bracket = gtk.Button("=calc[")
	open_bracket.set_tooltip_text("Вставить отрывающую скобку '=calc['")
        open_bracket.set_focus_on_click(False)
        #open_bracket.add( gtk.image_new_from_stock( gtk.STOCK_GO_BACK, gtk.ICON_SIZE_LARGE_TOOLBAR ) )
        open_bracket.connect("clicked", self.on_open_bracket_clicked)

	close_bracket = gtk.Button("    ]    ")
	close_bracket.set_tooltip_text("Вставить закрывающую скобку ']'")
        close_bracket.set_focus_on_click(False)
        #close_bracket.add( gtk.image_new_from_stock( gtk.STOCK_GO_FORWARD, gtk.ICON_SIZE_LARGE_TOOLBAR ) )
        close_bracket.connect("clicked", self.on_close_bracket_clicked)
	
	self.add_table_id = gtk.CheckButton( label = "Table ID")
	self.add_table_id.set_tooltip_text("Добавлять идентификатор таблицы к имени тега? (Для 62130-22ТР с идентификатором таблицы не добавлять, получение via eventTag__)")
	self.add_table_id.set_active( True )

	save = gtk.Button()
	save.set_tooltip_text("Сохранить в файл")
        save.set_focus_on_click(False)
        save.add( gtk.image_new_from_stock( gtk.STOCK_FLOPPY, gtk.ICON_SIZE_LARGE_TOOLBAR ) )
        save.connect("clicked", self.on_save_clicked)

	load = gtk.Button()
	load.set_tooltip_text("Сохранить в файл")
        load.set_focus_on_click(False)
        load.add( gtk.image_new_from_stock( gtk.STOCK_OPEN, gtk.ICON_SIZE_LARGE_TOOLBAR ) )
        load.connect("clicked", self.on_load_clicked)

	set_pos = gtk.Button()
	set_pos.set_tooltip_text("Установить новую позицию, добавлять будем после нее, затирая старую информацию")
        set_pos.set_focus_on_click(False)
        set_pos.add( gtk.image_new_from_stock( gtk.STOCK_REFRESH, gtk.ICON_SIZE_LARGE_TOOLBAR ) )
        set_pos.connect("clicked", self.on_set_pos_clicked)

	#ugly layout        
        self.L1.pack_start(gtk.Label("   "), False, False, 0)
        self.L2.pack_start(gtk.Label("                                    "), False, False, 0)

        self.L1.pack_start( move, False, False, 0)
        self.L1.pack_start( self.x_tag, False, False, 0)
        self.L1.pack_start( add_to_end, False, False, 0)
        self.L1.pack_start( self.y_tag, True, True, 0)

        self.L2.pack_start( self.add_table_id, False, False, 0)
        self.L2.pack_start(gtk.Label("                     "), False, False, 0)


        self.L2.pack_start( put_at_pos, False, False, 0)
        self.L2.pack_start( clear_pos, False, False, 0)
        self.L2.pack_start( new_group, False, False, 5)

        self.L2.pack_start(gtk.Label("   "), False, False, 0)

        frame_box.pack_start( logic_not, False, False, 0)
        frame_box.pack_start( logic_or, False, False, 0)
        frame_box.pack_start( logic_and, False, False, 0)

        frame_box.pack_start(gtk.Label("   "), False, False, 0)
        frame_box.pack_start( open_bracket, False, False, 0)
        frame_box.pack_start( close_bracket, False, False, 0)

	frame.add( frame_box )
	self.L2.pack_start(frame, False, False, 0)

        self.L2.pack_start(gtk.Label("   "), False, False, 0)
        self.L2.pack_start( pos_frame, False, False, 0)

        self.L2.pack_start( set_pos, False, False, 5)

        self.L2.pack_end( load, False, False, 10)
        self.L2.pack_end( save, False, False, 0)


        self.pack_start(self.L1, False, False, 0)
        self.pack_start(self.L2, False, False, 0)

    def on_y_tag_clear( self, entry, icon_pos, event ):
	self.y_tag.set_text("")
	self.y_name = ""
	
    def on_set_pos_clicked( self, button ):
	self.emit ("set-pos", self.x, self.y )
	self.x = 0
	self.y = 0
	self.pos_tag.set_text("pos: last") 

    def on_new_group( self, button ):
	self.emit ("new-group" )
	
    def on_save_clicked( self, button ):
	self.emit ("save" )

    def on_load_clicked( self, button ):
	self.emit ("load" )

    def on_logic_not_clicked(self, button ):
	text = self.y_tag.get_text()
	if (len(text) == 0):
	    return
	text += " !"
	self.y_tag.set_text( text)
	self.y_name = text

    def on_logic_or_clicked(self, button ):
	text = self.y_tag.get_text()
	if (len(text) == 0):
	    return
	text += " or "
	self.y_tag.set_text( text)
	self.y_name = text

    def on_logic_and_clicked(self, button ):
	text = self.y_tag.get_text()
	if (len(text) == 0):
	    return
	text += " and "
	self.y_tag.set_text( text)
	self.y_name = text

    def on_open_bracket_clicked(self, button ):
	text = self.y_tag.get_text()
	if (len(text) == 0):
	    return
	text += "=calc["
	self.y_tag.set_text( text)
	self.y_name = text

    def on_close_bracket_clicked(self, button ):
	text = self.y_tag.get_text()
	if (len(text) == 0):
	    return
	text += "]"
	self.y_tag.set_text( text)
	self.y_name = text

    def on_clear_pos_clicked(self, button ):
	if ( self.x != 0 and self.y != 0 ):
	    self.emit ("insert", "_del_", self.x, self.y )
	    self.x = 0
	    self.y = 0
    	    self.pos_tag.set_text("pos: last") 
	    self.y_tag.set_text("")
	
    def on_put_at_pos_clicked(self, button ):
	text = self.y_tag.get_text()
	self.y_tag.set_text("")
	if ( None != text and len(text) != 0 ):
	    if ( self.x != 0 and self.y != 0 ):
		self.emit ("insert", text, self.x, self.y )
	    else:
		self.emit ("append", text )

    def on_add_to_end_clicked(self, button ):
	text = self.y_tag.get_text()
	text += self.x_name
	self.y_tag.set_text( text )


    def on_move_clicked(self, button ):
	if ( self.x != 0 and self.y != 0 ):
    	    self.emit ("insert", self.x_name, self.x, self.y )
	    self.y_name = self.x_name
	    self.y_tag.set_text( self.x_name )
	    
	self.x = 0
	self.y = 0
	self.pos_tag.set_text("pos: last") 

    def set_table_id_from_x( self, obj, name ):
	self.table_id = name

    def set_tag_from_x( self, obj, name ):
	x_name = name
	if self.add_table_id.get_active() and len( self.table_id ):
	    x_name = self.table_id+"."+name

	if ( self.x != 0 and self.y != 0 ):
	    self.emit ("insert", x_name, self.x, self.y )
	    self.x = 0
	    self.y = 0
	else:
	    self.emit ("append", x_name )
    	self.y_tag.set_text( "" )
	self.pos_tag.set_text("pos: last") 

    def tag_from_x( self, obj, name ):
	self.x_name = name
	if self.add_table_id.get_active() and len( self.table_id ):
	    self.x_name = self.table_id+"."+name
	self.x_tag.set_text( self.x_name )

    def	tag_from_y( self, obj, name ):
	if ( name != None ):
	    self.y_name = name
	    self.y_tag.set_text( name )
#	else:
#	    self.y_name = None
#	    self.x = 0
#	    self.y = 0
#    	    self.pos_tag.set_text("pos: last") 
	    
    def	pos_from_y( self, obj, x, y  ):
	self.x = x
	self.y = y
	self.pos_tag.set_text("pos: %d:%d" % (self.x, self.y) )

	    