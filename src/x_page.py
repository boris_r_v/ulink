#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os, sys, pygtk, gtk.glade, gobject
import x_digital

class Page(gtk.VBox):
    __gsignals__ = {
        "choose-impl": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_STRING,)),
        "set-impl": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_STRING,)),
        "table-id": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_STRING,)),
    }

    def __init__( self, table ):
        gtk.VBox.__init__( self )
	self.scw = gtk.ScrolledWindow()
	self.view = x_digital.Context( )
	self.table_id = ""
	if "id" in table.attrib:
	    self.table_id = table.attrib["id"]
	
	for g in table:
	    for attr in ['i1', 'i2', 'i3', 'i4', 'i5', 'i6', 'i7', 'i8']:
		if ( attr in g.attrib ):
		    self.view.add_tag( g.attrib[ attr ] )
		else:
		    self.view.add_tag( "" )
	self.view.connect("choose-impl", self.on_choose_impl )
	self.view.connect("set-impl", self.on_set_impl )
	
	self.scw.add( self.view )
	self.pack_start( self.scw )
	self.show_all()
    
	
    def on_choose_impl( self, obj, name ):
	self.emit("table-id", self.table_id )
	self.emit("choose-impl", name )

    def on_set_impl( self, obj,  name ):
	self.emit("table-id", self.table_id )
	self.emit("set-impl", name )




	

	    