#!/usr/bin/env python
# -*- coding: utf-8 -*-


import pygtk, gtk.glade, gobject

class Chousen:
    def __init__( self, _iter, _pos, _text ):
	self.iter=_iter
	self.pos=_pos
	self.text=_text

class Context(gtk.TreeView):
    __gsignals__ = {
        "choose-impl": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_STRING,)),
        "choose-pos": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_INT, gobject.TYPE_INT,)),
        "nsi-current-text": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_STRING,)),
    }

    def __init__( self, impl2group ):
	gtk.TreeView.__init__( self, self.create_model( impl2group ) )
	self.set_grid_lines( gtk.TREE_VIEW_GRID_LINES_BOTH )        
	self.max_col = impl2group
	self.create()
	self.id4impl = {}
	self.nsi_group = -1
	self.nsi_group_old = -1


    def create( self ):
	self.renderer =  gtk.CellRendererText()
	self.chousen = Chousen( None, None, None )

	nc = self.append_column( gtk.TreeViewColumn( "Группа" , gtk.CellRendererText(), text=0 ) ) 
	colum = self.get_column( nc-1 ) 
	colum.set_expand(True)
	colum.set_sizing( gtk.TREE_VIEW_COLUMN_FIXED )

	for i in range(0, self.max_col):
		    #WARNING - не менять строку "i"+str(i+1)
	    nc = self.append_column( gtk.TreeViewColumn( "i"+str(i+1) , self.renderer, text=2*i+1, background=2*i+2 ) ) 
	    colum = self.get_column( nc-1 ) 
	    colum.set_expand(True)
	    colum.set_sizing( gtk.TREE_VIEW_COLUMN_FIXED )

	
	self.one_color = '#29AE97'
	self.zero_color = '#ffffff'
	self.get_selection().set_mode(gtk.SELECTION_NONE)
	self.connect("cursor_changed", self.on_cursor_changed )
	self.path = "";
	self.group = -1;
	self.impl = -2

    def create_model( self, impl2group ):
	if ( 20 == impl2group ):
	    self.store = gtk.ListStore( gobject.TYPE_STRING, #номер группы 0
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 1-impl, color 1-impl, 1.2
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 2-impl, color 2-impl, 3.4
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 3-impl, color 3-impl, 5.6
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 4-impl, color 4-impl, 7.8
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 5-impl, color 5-impl, 9.10
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 6-impl, color 6-impl, 11.12
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 7-impl, color 7-impl, 13.14
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 8-impl, color 8-impl, 15.16
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 9-impl, color 9-impl, 17.18
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 10-impl, color 10-impl, 19.20
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 11-impl, color 11-impl, 21.22
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 12-impl, color 12-impl, 23.24
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 13-impl, color 13-impl, 25.26
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 14-impl, color 14-impl, 27.28
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 15-impl, color 15-impl, 29.30
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 16-impl, color 16-impl, 31.32
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 17-impl, color 17-impl, 33.34
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 18-impl, color 18-impl, 35.36
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 19-impl, color 19-impl, 37.38
			    gobject.TYPE_STRING, gobject.TYPE_STRING )  # text 20-impl, color 20-impl, 39.40

	    self.nsi_store = gtk.ListStore( gobject.TYPE_STRING, #номер группы 0
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 1-impl, color 1-impl, 1.2
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 2-impl, color 2-impl, 3.4
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 3-impl, color 3-impl, 5.6
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 4-impl, color 4-impl, 7.8
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 5-impl, color 5-impl, 9.10
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 6-impl, color 6-impl, 11.12
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 7-impl, color 7-impl, 13.14
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 8-impl, color 8-impl, 15.16
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 9-impl, color 9-impl, 17.18
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 10-impl, color 10-impl, 19.20
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 11-impl, color 11-impl, 21.22
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 12-impl, color 12-impl, 23.24
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 13-impl, color 13-impl, 25.26
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 14-impl, color 14-impl, 27.28
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 15-impl, color 15-impl, 29.30
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 16-impl, color 16-impl, 31.32
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 17-impl, color 17-impl, 33.34
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 18-impl, color 18-impl, 35.36
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 19-impl, color 19-impl, 37.38
			    gobject.TYPE_STRING, gobject.TYPE_STRING )  # text 20-impl, color 20-impl, 39.40
	    return self.store    
	if ( 8 == impl2group ):
	    self.store = gtk.ListStore( gobject.TYPE_STRING, #номер группы 0
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 1-impl, color 1-impl, 1.2
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 2-impl, color 2-impl, 3.4
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 3-impl, color 3-impl, 5.6
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 4-impl, color 4-impl, 7.8
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 5-impl, color 5-impl, 9.10
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 6-impl, color 6-impl, 11.12
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 7-impl, color 7-impl, 13.14
			    gobject.TYPE_STRING, gobject.TYPE_STRING )  # text 8-impl, color 8-impl, 15.16
	    self.nsi_store = gtk.ListStore( gobject.TYPE_STRING, #номер группы 0
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 1-impl, color 1-impl, 1.2
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 2-impl, color 2-impl, 3.4
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 3-impl, color 3-impl, 5.6
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 4-impl, color 4-impl, 7.8
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 5-impl, color 5-impl, 9.10
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 6-impl, color 6-impl, 11.12
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 7-impl, color 7-impl, 13.14
			    gobject.TYPE_STRING, gobject.TYPE_STRING )  # text 8-impl, color 8-impl, 15.16
	    return self.store    
	return None

    def on_cursor_changed( self, pos ):
	cursor = self.get_cursor()
	if (cursor[1] == None ):
	    return
	if ( cursor[1].get_title()[0] != "i" ):
	    return
	if ( self.chousen.iter != None and self.chousen.pos != None ):
	    #remove old selection
	    self.get_model().set( self.chousen.iter, self.chousen.pos, self.zero_color )
	self.chousen.iter = self.get_model().get_iter( cursor[0] )
	#WARNING - предполаем что титле столбца вида iномер
	self.chousen.pos = 2*(int(cursor[1].get_title()[1:])-1)+2
	self.chousen.name = self.get_model().get( self.chousen.iter, self.chousen.pos-1 )[0]
	col = self.get_model().get(self.chousen.iter, self.chousen.pos )[0]
	if ( self.one_color == col ):
	    self.get_model().set( self.chousen.iter, self.chousen.pos, self.zero_color )
	else:
	    self.get_model().set( self.chousen.iter, self.chousen.pos, self.one_color )

	group = int(self.store.get( self.chousen.iter, 0 )[0])
	impl = int(cursor[1].get_title()[1:])
	#send nsi text title
	if ( self.nsi_group != -1 ):
	    self.nsi_group_old = self.nsi_group
	    self.nsi_impl_old = self.nsi_impl

        self.emit("choose-pos", group, impl )
	self.emit("choose-impl", self.chousen.name )

	self.nsi_group = self.get_model().get_string_from_iter( self.chousen.iter )
	self.nsi_impl = impl - 1
	nsi_iter = self.nsi_store.get_iter_from_string( str( self.nsi_group ) )
	title = self.nsi_store.get( nsi_iter, self.nsi_impl*2+2 )[0]
	self.emit("nsi-current-text", title )

    def check_group(self):
	try:
	    self.get_model().get_iter_from_string( str(self.group) )
	    return False
	except:
	    return True

    #Обновить содержимое в таблице НСИ, по текущим координатам, когда пользовтаель нажал Enter в поле ввода НСИ для данного сигнала
    def update_nsi( self, nsi_text ):
	nsi_iter = self.nsi_store.get_iter_from_string( str( self.nsi_group ) )
	self.nsi_store.set( nsi_iter, self.nsi_impl*2+2, nsi_text )

    #Обновить содержимое в таблице НСИ, по текущим координатам, когда пользователь сменил выбранный импульс
    def update_nsi_old( self, nsi_text ):
	if ( self.nsi_group_old == -1 ):
	    return
	nsi_iter = self.nsi_store.get_iter_from_string( str( self.nsi_group_old ) )
	self.nsi_store.set( nsi_iter, self.nsi_impl_old*2+2, nsi_text )


    #добавить импулсь к таблице
    def add_tag( self, name ):
	#Если пришел пустой тег - но с идентификатором таблицы ничего не делаем
	lst = name.split(".")
	if ( 2 == len(lst) and lst[1]==''):
	    return	
	if ( self.group == -1 ):
	    self.group = 0 
	    iter = self.get_model().append()
	    self.path = self.get_model().get_string_from_iter( iter )
	    self.get_model().set( iter, 0, str( self.group+1 ) )
	    self.impl = 0 
	    
	iter = self.get_model().get_iter_from_string( self.path )
	
	if ( -1 == self.impl ):	
	    self.group += 1	
	    if ( self.check_group() ):
		iter = self.get_model().append()
		self.path = self.get_model().get_string_from_iter( iter )
	    else:
		self.path = str(self.group)
		iter = self.get_model().get_iter_from_string( self.path )

	    self.get_model().set( iter, 0, str( self.group+1 ) )
	    self.impl = 0;

	#print self.impl, 2*self.impl+1, name, 2*self.impl+2, self.zero_color
	self.get_model().set( iter, 2*self.impl+1, name, 2*self.impl+2, self.zero_color )
	self.impl += 1;
	if ( self.impl == self.max_col ):
	    self.impl = -1

    def new_group( self ):
	self.group += 1	
	iter = self.get_model().append()
	self.path = self.get_model().get_string_from_iter( iter )
	self.get_model().set( iter, 0, str( self.group+1 ) )
	self.impl = 0;

    def insert_tag( self, name, group, impl ):
	iter = self.store.get_iter_from_string( str(group-1) )
	self.store.set_value( iter, 2*(impl-1)+1, name )


    def save( self, path ):
	ffile = open( path, "w")
	ffile.write( "<?xml version='1.0' encoding='UTF-8'?>\n" )
	if ( 20 == self.max_col ):
    	    ffile.write( "<ksa xmlns:gefest='http://nilksa.ru/xml-schema'>\n\n" )
	    ffile.write( "\t<gefest:TableDigital transport='net' >\n" )
	if ( 8 == self.max_col ):
	    ffile.write( "<ksa xmlns:ktsuk='http://nilksa.ru/xml-schema'>\n\n" )
	    ffile.write( "\t<ktsuk:Ts name='St.Name_PK' type='digital' link_table='1' debug='0' >\n" )
	iter = self.store.get_iter_first()
	while( iter ):    
	    group = self.store.get( iter, 0 )[0] 
	    ffile.write( "\t\t<g%s" % group )
	    for i in range(0, self.max_col ):
		impl = self.store.get( iter, i*2+1 )[0]
		if None == impl:
		    impl="" 
		if ( impl !="" and self.max_col == 8 and impl.find("=calc[") == -1 and len(impl.split(".") ) < 2 ):
		    if ( impl.decode("utf8") in self.id4impl ):
	    	        impl = self.id4impl[impl.decode("utf8")] + "." + impl
		    else:
			impl = "LINK." + impl	

	        ffile.write( " i%d='%s' " % (i+1, impl) )
	    ffile.write( "/>\n" )
	    iter = self.store.iter_next( iter )	

	if ( 20 == self.max_col ):
	    ffile.write( "\t</gefest:TableDigital>\n" )
	if ( 8 == self.max_col ):
	    ffile.write( "\t</ktsuk:Ts>\n" )

	self.save_nsi( ffile )

	ffile.write( "</ksa>\n" )
	return self.store

    def save_csv( self, path ):
	op = path
	if ( -1 == op.find("csv") ):
	    op += ".csv"
	ffile = open( path, "w")
	ffile.write( "Группа;Импульс;Имя;Описание;Выражение\n" )
	iter = self.store.get_iter_first()
	while( iter ):    
	    group = self.store.get( iter, 0 )[0] 
	    for i in range(0, self.max_col ):
		impl = self.store.get( iter, i*2+1 )[0]
		if ( None != impl and len(impl) != 0 ):
		    ffile.write( "%s;%d;%s;;\n" % (group, i+1, impl) )
	    iter = self.store.iter_next( iter )	

	ffile.close( )
	
	ffile = open( path+".table.csv", "w")
	ffile.write( "Группа;Импульс 1;Импульс 2;Импульс 3;Импульс 4;Импульс 5;Импульс 6;Импульс 7;Импульс 8\n" )
	iter = self.store.get_iter_first()
	while( iter ):    
	    group = self.store.get( iter, 0 )[0] 
	    ffile.write( "\n%s" % ( group ) )
	    for i in range(0, self.max_col ):
		impl = self.store.get( iter, i*2+1 )[0]
		if ( None != impl and len(impl) != 0 ):
		    ffile.write( ";%s" % (impl) )
	    iter = self.store.iter_next( iter )	

	ffile.close( )

	return self.store

    def save_nsi(self, ffile ):
	ffile.write("\n\t<gefest:NsiDigital debug='0'>\n" )
	iter = self.nsi_store.get_iter_first()
	while( iter ):    
	    group = self.nsi_store.get( iter, 0 )[0] 
	    ffile.write( "\t\t<g%s " % ( group ) )
	    for i in range(0, self.max_col ):
		impl = self.nsi_store.get( iter, i*2+1 )[0]
		title = self.nsi_store.get( iter, i*2+2 )[0]
		if ( None != impl and None != title and len(impl) != 0 ):
		    ffile.write( " i%d='%s' t%d='%s' " % (i+1, impl, i+1, title) )

	    ffile.write( " />\n" )
	    iter = self.nsi_store.iter_next( iter )	
    	ffile.write("\t</gefest:NsiDigital>\n")		            
	ffile.write( "\n" )

    
    def load( self, table ):
	self.store.clear()
	self.path = "";
	self.group = -1;
	self.impl = -2
	for g in table:
	    #for attr in [ 'i1', 'i2', 'i3', 'i4', 'i5', 'i6', 'i7', 'i8','i9', 'i10', 'i11', 'i12', 'i13', 'i14', 'i15', 'i16', 'i17', 'i18', 'i19', 'i20' ]:
	    for impl in range(0, self.max_col ):
		impl = "i"+str( impl+1 )
		if ( impl in g.attrib ):
		    attr = g.attrib[ impl ]
		    if ( 0 != len(attr) and self.max_col == 8  and attr.find("=calc[") == -1):
			lst = attr.split(".")
			if ( 2 == len(lst) ):
			    attr = lst[1]
			    self.id4impl[attr] = lst[0]
		    self.add_tag( attr )
		else:
		    self.add_tag( "" )

    def load_nsi( self, table ):
	self.nsi_store.clear()
	grp = 0
	for g in table:
	    grp += 1
	    iter = self.nsi_store.append()
	    self.nsi_store.set( iter, 0, str(grp) )
	    for impl in range(0, self.max_col ):
		impl_n = "i"+str( impl+1 )
		title_n = "t"+str( impl+1 )
		attr = ""
		title = ""
		if ( impl_n in g.attrib ):
		    attr = g.attrib[ impl_n ]
		    if ( 0 != len(attr) and self.max_col == 8  and attr.find("=calc[") == -1):
			lst = attr.split(".")
			if ( 2 == len(lst) ):
			    attr = lst[1]

		if ( title_n in g.attrib ):
		    title = g.attrib[ title_n ]

		self.nsi_store.set( iter, 2*impl+1, attr, 2*impl+2, title )
		
		    
    def set_pos( self, group, impl ):	
	self.path = str(group-1)
	self.group = group-1
	self.impl = impl-1

