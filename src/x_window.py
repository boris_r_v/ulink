#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os, sys, pygtk, gtk.glade
import x_tables, y_tables, x_control

class Window(gtk.Window):
	def __init__( self, args ):
	    gtk.Window.__init__( self )
	    self.set_title( "62130-22TP - digital maker" )  
	    self.set_default_size( 900, 600 )
	    box = gtk.VBox()

	    self.home_dir = os.getenv("PWD")

	    self.create_menu_bar( box )

	    self.mpk_notebook = x_tables.Tables( )
	    box.pack_start( self.mpk_notebook, True, True )

	    control = x_control.Control( )
	    box.pack_start( control, False, True )

	    self.uep_notebook = y_tables.Tables()
	    box.pack_start( self.uep_notebook, True, True )

	    self.mpk_notebook.connect( "choose-impl", control.tag_from_x )
	    self.mpk_notebook.connect( "set-impl", control.set_tag_from_x )
	    self.mpk_notebook.connect( "table-id", control.set_table_id_from_x )
	    self.uep_notebook.connect( "choose-impl", control.tag_from_y )
	    self.uep_notebook.connect( "choose-pos", control.pos_from_y )

	    control.connect( "append", self.uep_notebook.append_tag )
	    control.connect( "insert", self.uep_notebook.insert_tag )
	    control.connect( "new-group", self.uep_notebook.new_group)
	    control.connect( "set-pos", self.uep_notebook.set_pos)

	    control.connect( "save", self.on_save )
	    control.connect( "load", self.on_load_gefest )

	    self.add ( box )
	    self.connect("destroy", gtk.main_quit)
    
	def create_menu_bar( self, box ):
	    mb = gtk.MenuBar()
	    
	    filemenu = gtk.Menu()
	    filem = gtk.MenuItem("Файл")
	    filem.set_submenu( filemenu )
	    

	    new_table_8 = gtk.MenuItem("Создать таблицуу получатель МПК (ktsuk:Ts)")
	    new_table_20 = gtk.MenuItem("Создать таблицуу получатель 62130-22ТР (gefest:TableDigital)")
	    open_net_table = gtk.MenuItem("Открыть таблицу источник (МПК)")
	    open_gefest_table = gtk.MenuItem("Открыть таблицу получатель (МПК, 62130-22ТР)")
	    save_gefest_table = gtk.MenuItem("Сохранить таблицу получатель (МПК, 62130-22ТР) ")
	    save_gefest_csv = gtk.MenuItem("Экспорт табл. 62130-22ТР в CSV")
	    quit = gtk.MenuItem("Выход")

	    filemenu.append( new_table_8 )
	    filemenu.append( new_table_20 )
	    filemenu.append( gtk.SeparatorMenuItem() )
	    filemenu.append( open_net_table )
	    filemenu.append( open_gefest_table )
	    filemenu.append( gtk.SeparatorMenuItem() )
	    filemenu.append( save_gefest_table )
	    filemenu.append( save_gefest_csv )
	    filemenu.append( gtk.SeparatorMenuItem() )
	    filemenu.append( quit )
	    

	    new_table_8.connect( "activate", self.on_new_8 )
	    new_table_20.connect( "activate", self.on_new_20 )
	    open_net_table.connect( "activate", self.on_load_mpk )
	    open_gefest_table.connect( "activate", self.on_load_gefest )
	    save_gefest_table.connect( "activate", self.on_save )
	    save_gefest_csv.connect( "activate", self.on_save_csv )
	    quit.connect("activate", gtk.main_quit)

	    mb.append( filem )
	    box.pack_start( mb, False, True )

	def on_load_mpk( self, obj ):
	    dialog = gtk.FileChooserDialog( "Загрузить MPK из ...", action=gtk.FILE_CHOOSER_ACTION_OPEN, buttons=(gtk.STOCK_CANCEL, 0, gtk.STOCK_OK, 1) )
	    dialog.set_current_folder( self.home_dir )
	    ret = dialog.run()
	    if (1 == ret ): 	
	        if ( dialog.get_file() ):
		    self.mpk_notebook.set_file( dialog.get_file().get_path() )
	    dialog.destroy()	    

    
	def on_load_gefest( self, obj ):
	    dialog = gtk.FileChooserDialog( "Загрузить из ...", action=gtk.FILE_CHOOSER_ACTION_OPEN, buttons=(gtk.STOCK_CANCEL, 0, gtk.STOCK_OK, 1) )
	    dialog.set_current_folder( self.home_dir  )
	    ret = dialog.run()
	    if (1 == ret ): 	
	        if ( dialog.get_file() ):
		    self.uep_notebook.load( dialog.get_file().get_path() )
	    dialog.destroy()	    

	def on_save( self, obj ): 
	    dialog = gtk.FileChooserDialog( "Сохранить в xml", action=gtk.FILE_CHOOSER_ACTION_SAVE, buttons=(gtk.STOCK_CANCEL, 0, gtk.STOCK_OK, 1) )
	    dialog.set_current_folder( self.home_dir  )
	    ret = dialog.run()
	    if (1 == ret ): 	
	        if ( dialog.get_file() ):
		    self.uep_notebook.save( dialog.get_file().get_path() )
	    dialog.destroy()	    

	def on_save_csv( self, obj ): 
	    dialog = gtk.FileChooserDialog( "Сохранить в cvs", action=gtk.FILE_CHOOSER_ACTION_SAVE, buttons=(gtk.STOCK_CANCEL, 0, gtk.STOCK_OK, 1) )
	    dialog.set_current_folder( self.home_dir  )
	    ret = dialog.run()
	    if (1 == ret ): 	
	        if ( dialog.get_file() ):
		    self.uep_notebook.save_csv( dialog.get_file().get_path() )
	    dialog.destroy()	    

	def on_new_8( self, obj ):
	    self.uep_notebook.create_empty( 8 )

	def on_new_20( self, obj ):
	    self.uep_notebook.create_empty( 20 )


