#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys, pygtk, gtk.glade, gobject
import xml.etree.ElementTree as ET
import x_page, g_tab_label

class Tables(gtk.Notebook):
    __gsignals__ = {
        "choose-impl": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_STRING,)),
        "set-impl": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_STRING,)),
        "table-id": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_STRING,)),
    }
    
    def __init__( self ):
        gtk.Notebook.__init__( self )

    def set_file( self, pfile ):
	self.ns = {"Net": "http://nilksa.ru/xml-schema"}	
	try:
	    tree = ET.parse ( pfile )	
	    root = tree.getroot()
	    for table in root.findall('Net:ITable', self.ns):
		self.add_page( table.attrib["channelName"], table )
	except:
	    dialog = gtk.MessageDialog( message_format="Не верный формат файла ТС, неопределнный префик 'Net'. Добавте строку:\n<ksa xmlns:Net='http://nilksa.ru/xml-schema'>\n" )
	    dialog.show()


    def add_page( self, name, table ):
        page = x_page.Page( table )
	page.connect( "choose-impl", self.on_choose_impl )
	page.connect( "set-impl", self.on_set_impl )
	page.connect( "table-id", self.on_table_id )
	tab_label = g_tab_label.TabLabel( name )
	self.append_page( page, tab_label )
	tab_label.connect("close-clicked", self.on_close_page_clicked, page )
    
    def on_table_id( self, obj, name ):
        self.emit("table-id", name )

    def on_choose_impl( self, obj, name ):
        self.emit("choose-impl", name )

    def on_set_impl( self, obj, name ):
        self.emit("set-impl", name )

    def on_close_page_clicked(self, tab_label, page ):
        self.remove_page(self.page_num( page ))

