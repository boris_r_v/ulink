#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os, sys, pygtk, gtk.glade, gobject
import y_digital

class Page(gtk.VBox):
    __gsignals__ = {
        "choose-impl": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_STRING,)),
        "choose-pos": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_INT, gobject.TYPE_INT,)),
        "nsi-current-text": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_STRING,)),
    }

    def __init__( self  ):
        gtk.VBox.__init__( self )
	self.scw = gtk.ScrolledWindow()
	self.view = None
	self.pack_start( self.scw )
	self.show_all()
    
    def new_group( self ):
	if ( self.isView() ):
	    return 
	self.view.new_group( )

    def set_pos( self, x, y ):
	if ( self.isView() ):
	    return 
	if ( 0 != x and 0 != y ):
	    self.view.set_pos( x, y )
	
    def add_tag( self , name ):
	if ( self.isView() ):
	    return 
	if ( "" != name ):
	    self.view.add_tag( name )

    def insert_tag( self, name, x, y ):
	if ( self.isView() ):
	    return 
	if ( "" != name ):
	    self.view.insert_tag( name, x, y )
	if ( "_del_" == name ):
	    self.view.insert_tag( "", x, y )

    def on_impl_choose( self, obj, name ):
        self.emit("choose-impl", name )

    def on_nsi_current_text( self, obj, text ):
        self.emit("nsi-current-text", text )

    def on_pos_choose( self, obj, x, y ):
        self.emit("choose-pos", x, y )

    def update_nsi( self, nsi_text ):
	self.view.update_nsi( nsi_text )

    def update_nsi_old( self, nsi_text ):
	self.view.update_nsi_old( nsi_text )

    def save( self, path ):
	return self.view.save( path )

    def save_csv( self, path ):
	return self.view.save_csv( path )

    def isView (self ):
	if ( not self.view ):
	    dial = gtk.MessageDialog(message_format="Таблица получатель не создана.",  buttons=gtk.BUTTONS_CLOSE )
	    dial.run()
	    dial.destroy()
	    return True
	return False

    def create_context( self, imps2group ):
	if ( self.view ):
	    dial = gtk.MessageDialog(message_format="Таблица получатель уже создана, повторное создание невозможно.",  buttons=gtk.BUTTONS_CLOSE )
	    dial.run()
	    dial.destroy()
	    return

	self.view = y_digital.Context( imps2group )
	self.scw.add( self.view )
	self.scw.show_all()
	self.view.connect( "choose-impl", self.on_impl_choose )
	self.view.connect( "choose-pos", self.on_pos_choose )
	self.view.connect( "nsi-current-text", self.on_nsi_current_text )

    
    def load( self, table,  imps2group ):
        self.create_context( imps2group )
	return self.view.load( table )

    def load_nsi( self, table,  imps2group ):
	return self.view.load_nsi( table )
