#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys, pygtk, gtk.glade, gobject
import x_control, y_page
import xml.etree.ElementTree as ET


class Tables(gtk.VBox):
    __gsignals__ = {
        "choose-impl": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_STRING,)),
        "choose-pos": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_INT, gobject.TYPE_INT,)),
    }
    
    def __init__( self ):
        gtk.VBox.__init__( self )

	self.page = y_page.Page( )
	self.pack_start( self.page, True, True )	
	
	self.nsi_editor = gtk.Entry()
    	hbox = gtk.HBox()
	hbox.pack_start( gtk.Label("Пояснение к сигналу (НСИ):"), False, False )
	hbox.pack_start( self.nsi_editor, True, True )	
	self.pack_start( hbox, False, False )	

	self.page.connect( "choose-impl",  self.on_impl_choose )
	self.page.connect( "choose-pos", self.on_pos_choose )
	self.page.connect( "nsi-current-text", self.on_nsi_current_text )
	self.nsi_editor.connect( "activate", self.on_entry_activate )	

	self.x = -1
	self.y = -1

    def new_group( self, obj ):
	self.page.new_group( )

    def append_tag( self, obj, name ):
	self.page.add_tag( name )

    def insert_tag( self, obj, name, x, y ):
	self.page.insert_tag( name, x, y )

    def on_nsi_current_text( self, obj, text ):
	self.nsi_editor.set_text( text )

    def on_entry_activate( self, obj ):
	self.page.update_nsi( self.nsi_editor.get_text() )

    def set_pos( self, obj, x, y ):
	self.page.set_pos( x, y )

    def on_impl_choose( self, obj, name ):
        self.emit("choose-impl", name )

    def on_pos_choose( self, obj, x, y ):
	#Если сменили позицию - то надо обновить НСИ
	self.page.update_nsi_old( self.nsi_editor.get_text() )
	#Зашлем запрос далее
        self.emit("choose-pos", x, y )


    def save( self, path ):
	self.page.save( path )

    def save_csv( self, path ):
	self.page.save_csv( path )

    def create_empty( self, impl2group ):
	self.page.create_context( impl2group )

    def load( self, path ):
	tree = ET.parse ( path )	
	root = tree.getroot()
	for table in root.findall('gefest:TableDigital', {"gefest": "http://nilksa.ru/xml-schema"} ):
	    self.page.load ( table, 20 ) #20-импульсов в группе для увязки по Ethernet
	    break


	for table in root.findall('gefest:NsiDigital', {"gefest": "http://nilksa.ru/xml-schema"} ):
	    self.page.load_nsi ( table, 20 ) #20-импульсов в группе для увязки по Ethernet
	    break

	for table in root.findall('ktsuk:Ts', {"ktsuk": "http://nilksa.ru/xml-schema"} ):
	    self.page.load ( table, 8 ) #8-импульсов в группе для увязки по RS-422
	    break


