#!/usr/bin/env python
# -*- coding: utf-8 -*-


import pygtk, gtk.glade, gobject

	
class TabLabel(gtk.HBox):
    __gsignals__ = {
        "close-clicked": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, ()),
    }
    def __init__(self, label_text):
        gtk.HBox.__init__( self )
        self.set_spacing(5) # spacing: [label|5px|close]  
        
        # label 
	eventbox = gtk.EventBox()
	eventbox.connect( "button_press_event", self.on_press_event )
        self.label = gtk.Label(label_text)
	eventbox.add(self.label)
        self.pack_start(eventbox, True, True, 0)
        # close button
        button = gtk.Button()
        button.set_focus_on_click(False)
        button.add( gtk.image_new_from_stock( gtk.STOCK_CLOSE, gtk.ICON_SIZE_MENU ) )
        button.connect("clicked", self.on_close_clicked)
        self.pack_start(button, False, False, 0)
	self.show_all()	
            
    def on_close_clicked(self, button, data=None):
        self.emit("close-clicked")
	
    def on_press_event( self, button, event ):
	if ( event.button == 3 ):		
	    self.open_change_label_dialog()


    def open_change_label_dialog( self ):
        dialog = gtk.Dialog( "Имя канала", None, gtk.DIALOG_MODAL | gtk.DIALOG_DESTROY_WITH_PARENT,( gtk.STOCK_CANCEL, False, gtk.STOCK_OK, True ) )
	frame = gtk.Frame("Имя канала")    
	entry = gtk.Entry() 
	entry.set_text( self.label.get_label() )
	frame.add(entry)
	frame.show_all()
	dialog.vbox.pack_start(frame)
	if ( dialog.run() == False ):
	    dialog.destroy()
	    return
	
	self.label.set_label( entry.get_text() )
	dialog.destroy()

    def get_label(self):
	return self.label 