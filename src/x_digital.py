#!/usr/bin/env python
# -*- coding: utf-8 -*-


import pygtk, gtk.glade, gobject

class Chousen:
    def __init__( self, _iter, _pos, _text ):
	self.iter=_iter
	self.pos=_pos
	self.text=_text

class Context(gtk.TreeView):
    __gsignals__ = {
        "choose-impl": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_STRING,)),
        "choose-pos": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_INT, gobject.TYPE_INT,)),
        "set-impl": (gobject.SIGNAL_RUN_FIRST, gobject.TYPE_NONE, (gobject.TYPE_STRING,)),
    }

    def __init__( self ):
	gtk.TreeView.__init__( self, self.create_model() )
	self.set_grid_lines( gtk.TREE_VIEW_GRID_LINES_BOTH )        
	self.max_col = 8
	self.create()

    def create( self ):
	self.renderer =  gtk.CellRendererText()
	self.chousen = Chousen( None, None, None )

	nc = self.append_column( gtk.TreeViewColumn( "Группа" , gtk.CellRendererText(), text=0 ) ) 
	colum = self.get_column( nc-1 ) 
	colum.set_expand(True)
	colum.set_sizing( gtk.TREE_VIEW_COLUMN_FIXED )

	for i in range(0, self.max_col):
		    #WARNING - не менять строку "i"+str(i+1)
	    nc = self.append_column( gtk.TreeViewColumn( "i"+str(i+1) , self.renderer, text=2*i+1, background=2*i+2 ) ) 
	    colum = self.get_column( nc-1 ) 
	    colum.set_expand(True)
	    colum.set_sizing( gtk.TREE_VIEW_COLUMN_FIXED )

	
	self.one_color = '#29AE97'
	self.zero_color = '#ffffff'
	self.get_selection().set_mode(gtk.SELECTION_NONE)
	self.connect("cursor_changed", self.on_cursor_changed )
	self.connect("button-press-event", self.on_button_press_event )
	self.connect("key-press-event", self.on_key_press_event )
	self.path = "";
	self.group = -1;
	self.impl = -2

    def create_model( self ):
	self.store = gtk.ListStore( gobject.TYPE_STRING, #номер группы 0
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 1-impl, color 1-impl, 1.2
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 2-impl, color 2-impl, 3.4
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 3-impl, color 3-impl, 5.6
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 4-impl, color 4-impl, 7.8
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 5-impl, color 5-impl, 9.10
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 6-impl, color 6-impl, 11.12
			    gobject.TYPE_STRING, gobject.TYPE_STRING,  # text 7-impl, color 7-impl, 13.14
			    gobject.TYPE_STRING, gobject.TYPE_STRING )  # text 8-impl, color 8-impl, 15.16
	return self.store    

    def on_cursor_changed( self, pos ):
	cursor = self.get_cursor()
	if (cursor[1] == None ):
	    return
	if ( cursor[1].get_title()[0] != "i" ):
	    return
	if ( self.chousen.iter != None and self.chousen.pos != None ):
	    #remove old selection
	    self.get_model().set( self.chousen.iter, self.chousen.pos, self.zero_color )
	self.chousen.iter = self.get_model().get_iter( cursor[0] )	
	#WARNING - предполаем что титле столбца вида iномер
	self.chousen.pos = 2*(int(cursor[1].get_title()[1:])-1)+2
	self.chousen.name = self.get_model().get( self.chousen.iter, self.chousen.pos-1 )[0]
	col = self.get_model().get(self.chousen.iter, self.chousen.pos )[0]
	if ( self.one_color == col ):
	    self.get_model().set( self.chousen.iter, self.chousen.pos, self.zero_color )
	else:
	    self.get_model().set( self.chousen.iter, self.chousen.pos, self.one_color )

	group = int(self.store.get( self.chousen.iter, 0 )[0])
	impl = int(cursor[1].get_title()[1:])
        self.emit("choose-pos", group, impl )
        self.emit("choose-impl", self.chousen.name )

    #добавить импулсь к таблице
    def add_tag( self, name ):
	if ( self.group == -1 ):
	    self.group = 0 
	    iter = self.get_model().append()
	    self.path = self.get_model().get_string_from_iter( iter )
	    self.get_model().set( iter, 0, str( self.group+1 ) )
	    self.impl = 0 

	iter = self.get_model().get_iter_from_string( self.path )

	if ( -1 == self.impl ):	
	    self.group += 1	
	    iter = self.get_model().append()
	    self.path = self.get_model().get_string_from_iter( iter )
	    self.get_model().set( iter, 0, str( self.group+1 ) )
	    self.impl = 0;
	#print self.impl, 2*self.impl+1, name, 2*self.impl+2, self.zero_color
	self.get_model().set( iter, 2*self.impl+1, name, 2*self.impl+2, self.zero_color )
	self.impl += 1;
	if ( self.impl == self.max_col ):
	    self.impl = -1



    def on_button_press_event( self, obj, event ):
	if ( event.type == gtk.gdk._2BUTTON_PRESS ):
            self.emit("set-impl", self.chousen.name )

    def on_key_press_event( self, obj, event ):
	if ( event.type == gtk.gdk.KEY_PRESS and event.keyval == 65293 ):
            self.emit("set-impl", self.chousen.name )


