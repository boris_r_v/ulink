ulink
=====

*Редактор дискретной таблицы в формате 62130-22ТР или МПК.*

**Решаемые задачи:**

    - Перенос дискретных тегодв из таблицы в формате МПК в формат 62130-22ТР;
    - Сохранение, загрузка полученной таблицы
    - Формирвоание логических выражений


**Получение**

    1. echo "deb http://lin.ksa/std-mpk/deb-repo/ yout_debian_distr main" >> /etc/apt/sources.list
    2. apt-get update
    3. apt-get install ulink



**Для разработчика**
---------
Первый этап: **клонирование**

    git clone https://github.com/boris-r-v/ulink


**Форматы таблиц**
----------------

    <gefest:TableDigital debug='0' transport='net'>
    	<g1 i1="1СП" i2="!1з" i3="1РИ" i4="1МСП" i5="2СП" i6="!2з" i7="2РИ" i8="2МСП" i9="1П" i10="1и" i11="2П" i12="2и" i13="НП" i14="НПз" i15="ЧП" i16="ЧПз" i17="1+" i18="1-" i19="" i20="" 
	    i21="2+" i22="2-" i23="!1РИ or 1РИ and (2СП or !2з)" i24=""/>
	<g3 i1="1СП" i2="!1з" i3="1РИ" i4="1МСП" i5="2СП" i6="!2з" i7="2РИ" i8="2МСП" i9="1П" i10="1и" i11="2П" i12="2и" i13="НП" i14="НПз" i15="ЧП" i16="ЧПз" i17="1+" i18="1-" i19="" i20="" 
	    i21="2+" i22="2-" i23="" i24="" />
    </gefest:TableDigital>


    <ktsuk:Ts name='Salym_PK' type='digital' link_table='1' debug='0'>
	<g1 i1="kvfd.cvs-31-32_о" i2="kvfd.cvs-31-32_р" i3="цвс1=calc[kvfd.cvs-УК11_о or kvfd.cvs-УК11_р]" i4="цвс2=calc[kvfd.cvs-УМВ12_о or kvfd.cvs-УМВ12_р]" 
    	    i5="цвс3=calc[kvfd.cvs-УМВ13_о or kvfd.cvs-УМВ13_р]" i6="" i7="" i8="" />
    </ktsuk:Ts>